# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#g = Game.create room: 'room 1'
#p1 = Player.create username: 'player 1', password: '', disk_color: '#ff0000'
#p2 = Player.create username: 'player 2', password: '', disk_color: '#0000ff'
#gp1 = GamePlayer.create game: g, player: p1
#gp2 = GamePlayer.create game: g, player: p2
#PlayerMove.create game_player: gp1, index: 01
#PlayerMove.create game_player: gp1, index: 02
#PlayerMove.create game_player: gp1, index: 03
#PlayerMove.create game_player: gp1, index: 35
#PlayerMove.create game_player: gp2, index: 34

#g = Game.first
#p = g.players.first
#gp = GamePlayer.find_by game: g, player: p
#pm = PlayerMove.find_by game_player: gp, index: 1
#puts pm.game_player.player.id