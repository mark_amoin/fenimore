class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :username
      t.string :password
      t.string :disk_color
      t.integer :index

      t.timestamps null: false
    end
  end
end
