class CreatePlayerMoves < ActiveRecord::Migration
  def change
    create_table :player_moves do |t|
      t.integer :index
      t.belongs_to :game_player, index: true

      t.timestamps null: false
    end
  end
end
