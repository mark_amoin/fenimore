class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :room
      t.string :game_type, default: 'Player VS. Player'
      #t.column :status, "ENUM('Open', 'Started', 'Closed', 'Player Moved')"
      t.string :status, default: 'Open'
      t.integer :row, default: 6
      t.integer :column, default: 6
      #TODO: FKs.
      t.belongs_to :current_active_player, class_name: :Player, index: true, default: 1, validate: true
      t.belongs_to :winner, class_name: :Player, index: true, validate: true

      t.timestamps null: false
    end
  end
end
