# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160328105131) do

  create_table "game_players", force: :cascade do |t|
    t.integer  "game_id",    limit: 4
    t.integer  "player_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "game_players", ["game_id"], name: "index_game_players_on_game_id", using: :btree
  add_index "game_players", ["player_id"], name: "index_game_players_on_player_id", using: :btree

  create_table "games", force: :cascade do |t|
    t.string   "room",                     limit: 255
    t.string   "game_type",                limit: 255, default: "Player VS. Player"
    t.string   "status",                   limit: 255, default: "Open"
    t.integer  "row",                      limit: 4,   default: 6
    t.integer  "column",                   limit: 4,   default: 6
    t.integer  "current_active_player_id", limit: 4,   default: 1
    t.integer  "winner_id",                limit: 4
    t.datetime "created_at",                                                         null: false
    t.datetime "updated_at",                                                         null: false
  end

  add_index "games", ["current_active_player_id"], name: "index_games_on_current_active_player_id", using: :btree
  add_index "games", ["winner_id"], name: "index_games_on_winner_id", using: :btree

  create_table "player_moves", force: :cascade do |t|
    t.integer  "index",          limit: 4
    t.integer  "game_player_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "player_moves", ["game_player_id"], name: "index_player_moves_on_game_player_id", using: :btree

  create_table "players", force: :cascade do |t|
    t.string   "username",   limit: 255
    t.string   "password",   limit: 255
    t.string   "disk_color", limit: 255
    t.integer  "index",      limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

end
