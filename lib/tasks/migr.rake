namespace :migr do
  desc "Development clean migration and seeding."
  task :dev => :environment do
    Rails.env = 'development'
    ['drop', 'create', 'migrate', 'seed'].each do |s|
      Rake::Task["db:#{s}"].invoke
    end
  end
end