json.array!(@player_moves) do |player_move|
  json.extract! player_move, :id, :index
  json.url player_move_url(player_move, format: :json)
end
