json.array!(@game_players) do |game_player|
  json.extract! game_player, :id
  json.url game_player_url(game_player, format: :json)
end
