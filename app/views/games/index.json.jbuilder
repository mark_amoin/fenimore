json.array!(@games) do |game|
  json.extract! game, :id, :room, :game_type, :status, :row, :column
  json.url game_url(game, format: :json)
end
