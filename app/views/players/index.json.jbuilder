json.array!(@players) do |player|
  json.extract! player, :id, :username, :password, :disk_color
  json.url player_url(player, format: :json)
end
