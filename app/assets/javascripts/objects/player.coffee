class @Player
  constructor: (index, diskColor) ->
    @setDisk new Disk(diskColor)
    @setScore 0
    @setIndex index
    @setUsername ''
    @setPassword ''

  setDisk: (disk) ->
    @disk = disk

  getDisk: ->
    @disk

  setScore: (score) ->
    @score = score

  getScore: ->
    @score

  setIndex: (index) ->
    #One-based index.
    @index = index

  getIndex: ->
    @index

  setUsername: (username)->
    @username = username

  getUsername: ->
    @username

  setPassword: (password) ->
    @password = password

  getPassword: ->
    @password
    
  toJsonObject: ->
    {
      username: @getUsername(), 
      password: @getPassword(),
      disk_color: @getDisk().getColor(),
      index: @getIndex()
    }