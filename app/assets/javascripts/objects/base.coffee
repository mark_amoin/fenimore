#https://ice-cream-man-prezire.c9users.io/
#Sets up the game, UI and server interactions.
class @Base
  constructor: ->
    jQuery.sha256 = sha256
    @siteUrl = $('#server-urls').data 'site'
    @baseUrl = $('#server-urls').data 'base'
    @iServerUpdates = null
    @setListeners()

  generateRoom: ->
    s = $.sha256(new Date().toString())
    s.substring(0, 16);

  #Util.
  String::toInt = ->
    parseInt(this.match(/\d+/g)[0])

  showModal: (name, show) ->
    if !show
      $('.modal').modal 'hide'
      return false
    s = "##{name}-modal"
    o = $(s)
    switch name
      when 'waiting-opponent'
        $('#waiting-opponent-modal .modal-body span').html @g.getRoom()
        o.modal {
          backdrop: 'static',
          keyboard: false
        }
      when 'winner'
        o.modal()
        #TODO: Disable clicks except New Game btn.

  pingServerUpdates: (ping) ->
    if ping is false
      clearInterval @iServerUpdates
      return false
    ref = @
    @iServerUpdates = setInterval -> 
      ref.onPingServerUpdates()
    , 500

  onPingServerUpdates: ->
    ref = @
    $.post "#{@siteUrl}games/ping", {room: @g.getRoom()}, (response) ->
      r = response
      switch r.game.status
        when 'Started'
          players = r.players
          for p in players
            player = new Player p.index, p.disk_color
            player.setUsername p.username
            player.setPassword p.password
            ref.g.addPlayer player
          ref.showModal null, false
        when 'Open'
          ref.showModal 'waiting-opponent', true
        when 'Closed'
          ref.pingServerUpdates false
          data = 
          {
            room: ref.g.getRoom()
          }
          $.post "#{ref.siteUrl}games/winner", data, (response) ->
            winner = response.winner.username
            $('#winner-modal .modal-body span').html winner
            ref.showModal 'winner', true
        when 'Player Moved'
          #console.log 'player moved ', ref.g.getLocalPlayer().getIndex(), ref.g.getCurrentActivePlayer().getIndex() 
          #if ref.g.getLocalPlayer().getIndex() is ref.g.getCurrentActivePlayer().getIndex()
          #  console.log 'do nothing.'
          #else
          lm = r.latest_move;
          moveIndex = parseInt(lm.index)
          pos = ref.g.getBoard().getPosition moveIndex
          #console.log 'move ', r, pos, moveIndex
          ref.g.getBoard().occupyDisk pos.row,
            pos.column,
            true,
            r.disk_color
          
          lmo = r.latest_move_owner
          player = new Player lmo.index, 
            lmo.disk_color,
          player.setUsername lmo.username
          player.setPassword lmo.password
          #console.log 'moved. changing player ', player
          currPlayerId = r.game.current_active_player_id
          nextPlayer = ref.g.getPlayer currPlayerId
          ref.g.setCurrentActivePlayer nextPlayer
          #console.log 'active player ', ref.g.getCurrentActivePlayer()
          ref.updateLocalUi()
        when 'Ongoing'
          ref.g.start true
          ref.showModal null, false
  
  setListeners: ->
    ref = @
    $('#fenimore #game.play').on 'click', 'a.cancel-game', (e) ->
      e.preventDefault()
      ref.pingServerUpdates true

    $('#fenimore #board').on 'click', 'a.disk', (e) ->
      e.preventDefault()
      ref.onDiskClick this 
    
    $('#fenimore nav #play-game').on 'click', (e) ->
      e.preventDefault()
      #Stop prev interval before initing a new game.
      ref.pingServerUpdates false
      if $('#open-games option:selected').index() < 1
        ref.onNewGame() 
      else 
        ref.onJoinGame();

  onDiskClick: (target) ->
    #console.log 'disk click', @g.getLocalPlayer(), @g.getCurrentActivePlayer()
    ###
    if @g.getLocalPlayer().getIndex() isnt @g.getCurrentActivePlayer().getIndex()
      console.log 'local player move unavailable.'
      return false
    ###
    diskUi = $(target)
    diskIndex = diskUi.attr('class').toInt()
    blockUi = diskUi.parent()
    rowUi = blockUi.parent()
    rowUiIndex = rowUi.attr('class').toInt()
    blockUiIndex = blockUi.attr('class').toInt()
    p = @g.getCurrentActivePlayer()
    disk_color = p.getDisk().getColor()
    bOccupied = @g.getBoard().occupyDisk rowUiIndex,
      blockUiIndex,
      true,
      disk_color
    if bOccupied
      if @g.getHasWinner diskIndex
        @g.start false
        #@showModal 'winner', true
        @g.setNextActivePlayer()
        data =
        {
          room: @g.getRoom(),
          owner: p.toJsonObject(),
          next_active_player: @g.getCurrentActivePlayer().toJsonObject()
          disk_index: diskIndex
        }
        ref = @
        $.post "#{@siteUrl}games/move", data, ->
          data = 
          {
            room: ref.g.getRoom(),
            player: p.toJsonObject()
          }
          $.post "#{ref.siteUrl}games/stop", data
      else
        #The previous player's move is done. 
        #Set to next player.
        @g.setNextActivePlayer()
        data =
        {
          room: @g.getRoom(),
          owner: p.toJsonObject(),
          next_active_player: @g.getCurrentActivePlayer().toJsonObject()
          disk_index: diskIndex
        }
        $.post "#{@siteUrl}games/move", data
    else
      #Do nothing.
  
  onJoinGame: ->
    joiningPlayer = new Player 2, '#0000ff'
    #joiningPlayer.setUsername $('#fenimore #game.play #username').val()
    #joiningPlayer.setPassword $('#fenimore #game.play #password').val()
    #2nd player fixture.
    joiningPlayer.setUsername 'Player 2'
    joiningPlayer.setPassword 'y'
    data = 
      {
        room: $('#open-games option:selected').text(),
        player: joiningPlayer.toJsonObject()
      }
    ref = @
    $.post "#{@baseUrl}/games/join", data, (response) ->
      oGame = response.game
      oPlayers = response.players
      players = []
      for p in oPlayers
        pl = new Player p.index, p.disk_color
        pl.setUsername p.username
        pl.setPassword p.password
        players.push pl
      ref.initGame oGame.type, oGame.room, players, null
      ref.g.setLocalPlayer joiningPlayer
      ref.updateLocalUi()
      ref.pingServerUpdates true
  
  onNewGame: ->
    type = $('#fenimore nav #game-type option:selected').text()
    creator = new Player 1, '#ff0000'
    creator.setUsername $('#fenimore #username').val()
    creator.setPassword $('#fenimore #password').val()
    @initGame type, @generateRoom(), [creator], creator
    @g.setLocalPlayer creator
    data =
    {
      room: @g.getRoom(),
      player: creator.toJsonObject()
    }
    ref = @
    $.post "#{@siteUrl}games/start", data, ->
      ref.updateLocalUi()
      ref.pingServerUpdates true

  updateLocalUi: ->
    $('#fenimore #room span').html @g.getRoom()
    $('#fenimore #board').show()
    #$('#fenimore #current-player div').html @g.getCurrentActivePlayer().getUsername()

  initGame: (type, room, players, currentActivePlayer) ->
    @g = new Game()
    @g.setType type
    @g.setRoom room
    @g.addPlayer p for p in players
    @g.setBoard new Board $('#fenimore #board')
    @g.setCurrentActivePlayer currentActivePlayer

$(document).ready ->
  new Base()