class @Board
  constructor: (@ui) ->
    #Clear prev games.
    @ui.html ''
    @dimension = 6
    @min = 0
    @max = (@dimension * @dimension) - 1
    @render()

  getPosition: (index) ->
    d = @getDimension()
    {row: Math.floor(index / d), column: Math.floor(index % d)}

  getUi: ->
    @ui

  getDimension: ->
    @dimension

  getModel: ->
    #Get model based on UI to be used
    #for submitting data to server.
    @model = []
    for h in [0...@dimension]
      @model[h] = []
      for v in [0...@dimension]
        @model[h][v] = 0


  getWithinDimLimits: (index) ->
    index >= @min and index <= @max

  getCanOccupyDisk: (row, column) ->
    ui = @getDiskUi row, column
    index = parseInt(ui.attr('class').toInt())
    if @getWithinDimLimits index is true
      #Check bottom.
      i = index + @dimension
      bottomUi = @ui.find(".disk-#{i}")
      (i > @max) or bottomUi.hasClass 'occupied'

  #Rename to DiskSlot.
  occupyDisk: (row, column, occupied, diskColor) ->
    s = 'style'
    span = @getDiskUi row, column
    canOccupy = @getCanOccupyDisk row, column
    if occupied is true and !span.attr(s) and canOccupy
      span.addClass 'occupied'
      span.attr s, "background-color: #{diskColor}"

  getDiskUiIsEmpty: (index) ->
    ui = @getDiskUiByIndex index
    !ui.hasClass 'occupied'

  getDiskUiByIndex: (index) ->
    @ui.find("a.disk-#{index}")

  getDiskUi: (row, column) ->
    @ui.find(".row-#{row}").
      find(".column-#{column}").
      find('a')

  getDiskUiIndex: (row, column) ->
    parseInt(@getDiskUi(row, column).attr('class').toInt())

  render: ->
    i = 0
    for h in [0...@dimension]
      rowCntr = $('<div></div>').appendTo @ui
      rowCntr.addClass "row row-#{h}"
      for v in [0...@dimension]
        slot = $('<span></span>').appendTo rowCntr
        slot.addClass "slot block column-#{v}"
        $('<a href="#"></a>').appendTo(slot).addClass "disk disk-#{i}"
        i++
    ###
    dimRow = @dimension, dimCol = @dimension
    list = new Array(dimRow);
    for(index = 0, row = 0, col = 0;
      index < dimRow * dimCol;
      index++, col = index % dimCol, col < 1 ? row++ : row)
      if(!list[row]) list[row] = new Array(dimCol)
      list[row][col] = index
    ###