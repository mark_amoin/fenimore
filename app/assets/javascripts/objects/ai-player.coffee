##= require ./player

class @AiPlayer extends @Player
  constructor: (index, diskColor, @difficultyLevel) ->
    super index, diskColor
    
  setDifficultyLevel: (level) ->
    @difficultyLevel = level

  getDifficultyLevel: ->
    @difficultyLevel

  setAnalyzableBoard: (board) ->
    @board = board

  analyzeBoard: ->
   m = @board.getModel()
   #TODO:...

  move: ->
    @analyzeBoard()