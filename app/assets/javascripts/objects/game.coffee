class @Game
  constructor: ->
    ###Const statics
    @STATUS_STARTED = 'Started'
    @STATUS_OPEN = 'Open'
    @STATUS_CLOSED = 'Closed'
    @STATUS_PLAYER_MOVED = 'Player Moved'
    @STATUS_ONGOING = 'Ongoing'
    ###
    @aPlayers = []
    @setCurrentActivePlayer null
    @setLocalPlayer null
    @board = null
    @room = null
    @iMaxWinningColors = 4

  setRoom: (room) ->
    @room = room

  getRoom: ->
    @room

  addPlayer: (player) ->
    found = false
    if @aPlayers.length < 2
      for p in @aPlayers
        if p.getIndex() is player.getIndex()
          found = true
      if !found then @aPlayers.push player
    !found

  removePlayer: ->
    #Do nothing.

  getPlayer: (index) ->
    #@val -1 Zero-based index.
    @aPlayers[index - 1]

  getPlayers: ->
    @aPlayers.slice()

  setCurrentActivePlayer: (player) ->
    @currentActivePlayer = player

  getCurrentActivePlayer: ->
    @currentActivePlayer
    
  setLocalPlayer: (player) ->
    @localPlayer = player
    
  getLocalPlayer: ->
    @localPlayer

  setNextActivePlayer: ->
    #Sequentially selects the next player.
    j = @getCurrentActivePlayer().getIndex()
    i = j + 1
    if i > @aPlayers.length
      #@val 1 The original starting index of a player.
      i = 1
    nextPlayer = @getPlayer i
    @setCurrentActivePlayer nextPlayer

  setBoard: (board) ->
    @board = board

  getBoard: ->
    @board

  start: (isStart) ->
    if isStart is true
      #
    else
      #Disable all clicks.
      #Show winner...

  rgb2hex: (rgbString) ->
    rgbString = rgbString + ''
    rgb = rgbString.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
    if (rgb && rgb.length is 4) then "#" +
      ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
      ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
      ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) else ''

  getIsSameColor: (diskUiIndex) ->
    ui = @getBoard().getDiskUiByIndex diskUiIndex
    currActivePlayerDiskClr = @getCurrentActivePlayer().getDisk().getColor()
    s = ui.css 'background-color'
    c = @rgb2hex s
    currActivePlayerDiskClr is c

  getHasWinner: (index) ->
    pos = @getBoard().getPosition index
    @currentDiskRow = pos.row
    @currentDiskCol = pos.column
    @boardDim = @getBoard().getDimension()
    h = @checkHorizontal()
    v = @checkVertical()
    d = @checkDiagonals index
    h or v or d

  checkHorizontal: ->
    score = 0
    hasBroken = false
    for i in [0...@boardDim]
      j = @getBoard().getDiskUiIndex @currentDiskRow, i
      if !@getBoard().getDiskUiIsEmpty j
        if @getIsSameColor j
          score++
          #Bug horiz during broken colors.
          #hasBroken = false
          #if score >= @iMaxWinningColors and !hasBroken then return true
          if score >= @iMaxWinningColors then return true
        else
          hasBroken = true
    score >= @iMaxWinningColors and !hasBroken

  checkVertical: ->
    score = 0
    for i in [0...@boardDim]
      j = @getBoard().getDiskUiIndex i, @currentDiskCol
      if !@getBoard().getDiskUiIsEmpty j
        if @getIsSameColor j
          score++
          if score >= @iMaxWinningColors and !hasBroken then return true
        else
          hasBroken = true
    score >= @iMaxWinningColors and !hasBroken

  checkDiagonals: (index) ->
    distanceRight = @boardDim - @currentDiskCol
    distanceBottom = @boardDim - @currentDiskRow

    leftJumpStep = @boardDim + 1
    rightJumpStep = @boardDim - 1

    topLeftIndex = leftJumpStep * @currentDiskCol
    l = index - topLeftIndex
    if l < 0
      l = distanceBottom - distanceRight
    if l is index
      l = 0

    topRightIndex = rightJumpStep * (distanceRight - 1)
    r = index - topRightIndex
    if r < rightJumpStep
      r = @currentDiskRow + (@boardDim - distanceRight)
    if r is index
      r = 0

    leftPos = @getBoard().getPosition l
    rightPos = @getBoard().getPosition r
    halfBoard = (@boardDim - 1) / 2
    leftScore = rightScore = 0
    hasBroken = false
    #Track from left all the way down to the right.
    if leftPos.row < halfBoard and leftPos.column < halfBoard
      for i in [0...@boardDim - leftPos.row]
        forwardIndex = i *= leftJumpStep
        forwardPos = @getBoard().getPosition forwardIndex
        k = @getBoard().getDiskUiIndex forwardPos.row, forwardPos.column
        if !@getBoard().getDiskUiIsEmpty k
          if @getIsSameColor k
            leftScore++
            if leftScore >= @iMaxWinningColors and !hasBroken then return true
          else
            hasBroken = true
        
    #Track from right all the way down to the left.
    if rightPos.row < halfBoard and rightPos.column >= halfBoard
      startingIndex = @getBoard().getDiskUiIndex rightPos.row, rightPos.column
      for i in [0...@boardDim - rightPos.row]
        forwardIndex = startingIndex + (i *= rightJumpStep)
        forwardPos = @getBoard().getPosition forwardIndex
        k = @getBoard().getDiskUiIndex forwardPos.row, forwardPos.column
        if !@getBoard().getDiskUiIsEmpty k
          if @getIsSameColor k
            rightScore++
            if rightScore >= @iMaxWinningColors then return true
          else
            hasBroken = true
        
    (leftScore >= @iMaxWinningColors or rightScore >= @iMaxWinningColors) and !hasBroken

  setType: (type) ->
    @type = type

  getType: ->
    @type