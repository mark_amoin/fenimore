class GamePlayer < ActiveRecord::Base
    belongs_to :game
    belongs_to :player
    
    has_many :player_moves
end
