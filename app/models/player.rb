class Player < ActiveRecord::Base
    has_many :game_players
    has_many :games, through: :game_players
    has_many :player_moves, through: :game_players
end
