class Game < ActiveRecord::Base
    has_many :game_players
    has_many :players, through: :game_players
    belongs_to :current_active_player, class_name: :Player, validate: true
    belongs_to :winner, class_name: :Player, validate: true
end
