class GamesController < ApplicationController
  before_action :set_game, only: [:show, 
    :edit, 
    :update, 
    :destroy
  ]

  # GET /games
  # GET /games.json
  def index
    @games = Game.all
  end

  # GET /games/1
  # GET /games/1.json
  def show
  end
  
  # POST /games/play
  def stop
    p = params[:player]
    player = Player.find_by_username_and_password p[:username], 
      p[:password]
    game = Game.find_by_room params[:room]
    game.status = 'Closed'
    game.winner = player
    game.save
    render json: {game: game, winner: player}
  end
  
  # POST /games/winner
  def winner
    game = Game.find_by_room params[:room]
    player_id = game.winner
    player = Player.find player_id
    render json: {game: game, winner: player}
  end
  
  # GET /games/play
  def play
    @open_games = Game.where status: 'Open'
    render file: 'games/play'
  end
  
  def move
    game = Game.find_by_room params[:room]
    game.status = 'Player Moved'
    
    nap =  params[:next_active_player]
    nextPlayer = Player.find_by_username_and_password nap[:username],
      nap[:password]
    game.current_active_player = nextPlayer
    game.save
    
    p = params[:owner]
    username = p[:username]
    password = p[:password]
    disk_index = params[:disk_index]
    player = Player.find_by_username_and_password username, 
      password
    
    game_player = GamePlayer.find_by game: game, player: player
    player_move = PlayerMove.create game_player: game_player, 
      index: disk_index
    
    o = 
    {
      game: game,
      player_move: player_move, 
      latest_move_owner: player
    }
    render json: o
  end
  
  # POST /games/start
  def start
    p = params[:player]
    player = Player.find_or_create_by username: p[:username], 
      password: p[:password]
    player.disk_color = p[:disk_color]
    player.index = p[:index]
    player.save
    
    game = Game.create room: params[:room]
    game.players << player
    #game_player = GamePlayer.create game: game, player: player
    game_player = GamePlayer.find_by game: game, player: player
    
    o = {
      game: game, 
      game_player: game_player, 
      players: game.players
    }
    render json: o
  end
  
  #TODO: Refactor using start().
  # POST /games/join
  def join
    p = params[:player]
    player = Player.find_or_create_by username: p[:username], 
      password: p[:password],
      disk_color: p[:disk_color],
      index: p[:index]
    
    game = Game.find_by_room params[:room]
    game.status = 'Started'
    game.players << player
    game.save
    
    render json: to_json(params[:room])
  end
  
  # POST /games/ping
  def ping
    render json: to_json(params[:room])
  end

  # GET /games/new
  def new
    @game = Game.new
  end

  # GET /games/1/edit
  def edit
  end

  # POST /games
  # POST /games.json
  def create
    @game = Game.new(game_params)

    respond_to do |format|
      if @game.save
        format.html { redirect_to @game, notice: 'Game was successfully created.' }
        format.json { render :show, status: :created, location: @game }
      else
        format.html { render :new }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /games/1
  # PATCH/PUT /games/1.json
  def update
    respond_to do |format|
      if @game.update(game_params)
        format.html { redirect_to @game, notice: 'Game was successfully updated.' }
        format.json { render :show, status: :ok, location: @game }
      else
        format.html { render :edit }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /games/1
  # DELETE /games/1.json
  def destroy
    @game.destroy
    respond_to do |format|
      format.html { redirect_to games_url, notice: 'Game was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game
      #TODO: Check :resource in routes.
      @game = Game.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_params
      params.require(:game).permit(:room, :game_type, :status, :row, :column)
    end
    
    def to_json room
      game = Game.find_by_room room
      #TODO: Refactor.
      sql = "SELECT pm.* FROM player_moves pm 
        INNER JOIN game_players gp ON gp.id = pm.game_player_id 
        INNER JOIN games g ON gp.game_id = g.id 
        WHERE g.id = ? 
        ORDER BY pm.id DESC LIMIT 1"
      player_move = PlayerMove.find_by_sql([sql, game.id])[0]
      if player_move.nil?
        latest_move_owner = []
        disk_color = []
      else
        latest_move_owner = GamePlayer.find player_move.game_player_id
        disk_color = latest_move_owner.player.disk_color
      end
      #/TODO: Refactor.
      {
        game: game, 
        players: game.players, 
        latest_move_owner: latest_move_owner,
        latest_move: player_move,
        disk_color: disk_color
      }
    end
end
