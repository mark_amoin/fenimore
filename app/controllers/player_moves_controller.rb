class PlayerMovesController < ApplicationController
  before_action :set_player_move, only: [:show, :edit, :update, :destroy]

  # GET /player_moves
  # GET /player_moves.json
  def index
    @player_moves = PlayerMove.all
  end

  # GET /player_moves/1
  # GET /player_moves/1.json
  def show
  end

  # GET /player_moves/new
  def new
    @player_move = PlayerMove.new
  end

  # GET /player_moves/1/edit
  def edit
  end

  # POST /player_moves
  # POST /player_moves.json
  def create
    @player_move = PlayerMove.new(player_move_params)

    respond_to do |format|
      if @player_move.save
        format.html { redirect_to @player_move, notice: 'Player move was successfully created.' }
        format.json { render :show, status: :created, location: @player_move }
      else
        format.html { render :new }
        format.json { render json: @player_move.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /player_moves/1
  # PATCH/PUT /player_moves/1.json
  def update
    respond_to do |format|
      if @player_move.update(player_move_params)
        format.html { redirect_to @player_move, notice: 'Player move was successfully updated.' }
        format.json { render :show, status: :ok, location: @player_move }
      else
        format.html { render :edit }
        format.json { render json: @player_move.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /player_moves/1
  # DELETE /player_moves/1.json
  def destroy
    @player_move.destroy
    respond_to do |format|
      format.html { redirect_to player_moves_url, notice: 'Player move was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_player_move
      @player_move = PlayerMove.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def player_move_params
      params.require(:player_move).permit(:index)
    end
end
